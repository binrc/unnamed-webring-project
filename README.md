# Standard for a fully decentralized webring

This is a peer to peer webring. It has no title as of yet because the previous title was vaguely phallic. 

This proposal is fairly simple but this is intentional. Instaed of arguing over licensing and languages users are free (and encouraged) to create their own implementations that adhere to and expand upon this standard. 

## The fediverse killer

1. no ddos (like happens in the fediverse). 

Fediverse is an amplification machine. Every time a user creates a post, every server that is aware of their own server will begin to stampede. This causes major load issues and requires a very powerful computer to run even a small instance. 

2. not real time. 

This system is not time sensitive. It is designed to only query peers on a daily timer (with cron or similar). If the list becomes too large, clients can easily be modified to self rate limit. Instead of querying all of the servers sequentially the queries can be split up throughout the day. 
The system leverages data precomputed by trusted peers to further reduce the number of network requests.

3. Actually Private.

When you post to federated platforms your post is cached to hundreds if not thousands of servers that you do not own. All of these servers have the ability to permanently store anything you broadcast over the network. Some of the instances in a fediverse network will even submit any posts they find to advertisers. 

In a true decentralized system nuking **your own webserver that you operate** will sufficiently remove any and all data that you want to be removed. This will result in dead links and 404s as your "record" in the network is no longer accurate. 

4. Actually fully decentralized. 

Federation is feudal serfdom. Even if you *do* run your own instance you are still at the mercy of larger instances who dictate whether or not you can federate with large portions of the network. 


## the webring killer

1. no central point of entry
2. no central point of failure
3. Anyone can join the network without agreeing to align themselves with the arbitrary requirements, code of conduct, or ideology of an existing webring. 


# Indexes

## Advertising yourself

Your server can advertise its own details, such as its name, description and rss feed location, to other peers. This is a direct method other peers have to verify the details of your website, when needed.

Your webserver has to serve a file named `self.txt` located in the webroot directory. This is a plaintext file written according to this specification.

REQUIRED VALUES include "title", "description", and "url". Any lines that contain "://" are to be treated as a url. Any lines without a "://" are to be treated as plaintext and should be prefixed with their key in order to provide context. The url field MUST NOT end with a trailing slash.

The file is a key-value pairs list, with no empty lines.

Example:

```
title: John's Website
description: Cool recipes for dinner cooking
url: https://websitename.something
```

Other peers will fetch it to build their `1hop.txt` list, as explained in the next section. It's possible for other peers to fetch it if they suspect the details received by other peers have been tampered with.

This file doesn't need to be generated frequently. It can be rebuilt only when its details have to be changed.


## Nodes at one hop distance

Each node has a list of "friends websites", which are daily fetched to check if the advertised details have changed. Each node generates a list of friends nodes details, by concatenating the content of `self.txt` fetched by each one of them, as detailed in this section.
This information is saved in a plaintext file, named `1hop.txt`, located in the webroot directory.

The `1hop.txt` can be conceived as "a list of links to my friends". This establishes trust and helps prevent spam on the network. 

It is essentially a link list made up of key:value pairs. Individual entries are separated by a double newline "\n\n" (i.e. an empty line). The file should always end with a double newline in order to make it easier for users writing implementations in whichever language they choose. 

REQUIRED VALUES include "title", "description", and "url". Any lines that contain "://" are to be treated as a url. Any lines without a "://" are to be treated as plaintext and should be prefixed with their key in order to provide context. 

1hop.txt **SHOULD NOT** contain the following characters anywhere in the file: `< > & ' "`. These are reserved HTML characters and should be escaped using their corresponding HTML entities: `&lt; &gt; &amp; &quot; &apos`. The only exception to this requirement is that the `&` character can only be included in lines that also contain a "://" to indicate that they are a URL. This exception exists only to support those users who serve multiple RSS feeds and differentiate between them using GET requests. 

Example: 

```
title: 0x19.org
description: Unhinged UNIX posting.
url: https://0x19.org
rss: https://0x19.org/posts/feed.php
tor: http://ilsstfnqt4vpykd2bqc7ntxf2tqupqzi6d5zmk767qtingw2vp2hawyd.onion:8080
i2p: http://xzh77mcyknkkghfqpwgzosukbshxq3nwwe2cg3dtla7oqoaqknia.b32.i2p:9090
git: https://gitlab.com/binrc
foo: bar

title: Futility(1)
description: Unhinged 9 posting. 
url: https://9.0x19.org

```

An html rendering of the above could result in the following: 

```
<a href="https://0x19.org">0x19.org</a><br>
<span>description: Unhinged UNIX posting.</span><br>
<a href="https://0x19.org/posts/feed.php">rss</a></br>
<a href="http://ilsstfnqt4vpykd2bqc7ntxf2tqupqzi6d5zmk767qtingw2vp2hawyd.onion:8080">tor</a><br>
<a href="http://xzh77mcyknkkghfqpwgzosukbshxq3nwwe2cg3dtla7oqoaqknia.b32.i2p:9090">i2p</a><br>
<a href="https://gitlab.com/binrc">git</a><br>
<span>foo: bar</span><br>
<br>
<a href="https://9.0x19.org">Futility(1)</a><br>
<span>description: Unhinged 9 posting.</span><br>
<br>
```

Of course, individual implementations are free to render this text in any way. The above is merely an example to demonstrate how to handle the required lines and how to handle the additional lines. 

## 2hop.txt

2hop.txt can be conceived as "Links to the friends of my friends". This helps with discoverability without throwing trust to the wayside. 

The format of 2hop.txt is identical to 1hop.txt. This file will be created by periodically querying all of the hosts in your own 1hop.txt file. 2hop.txt is a deduplicated summation 

# How the network actually works

Every member of the webring is a *peer* in the network. This means that peering with others is a requirement for joining.  Every peer is its own authority on it's own record. This enables peers to modify their links and titles and mitigates the damage done by spoofing or hacking. 

Our own server is also a peer in the network. This will be called a *client* from here on out. 

Other servers in the network will be called *servers* from here on out. 

hop1 peers are the most trusted peers. This will be called ring1 from here on out. 

hop2 peers are only trusted because we trust our hop1 peers to not give us links to malware. This will be called ring2 from here on out. 

Additional hops and rings can be named and numbered accordingly. Only rings 1 and 2 are discussed here but the number of rings can be exponential. Ideally, a client should not do too many hops as trust is lost with each additional hop. Malware and links to malware can be quickly introduced into the network this way. 
Moreover, including too many hops or too many elements per list will result in including almost the whole network, which is in contradiction to the goal of rings, i.e. showing an interesting and consistent subset of the network.

1. First, make sure the client `self.txt` is correct, so other peers will be able to fetch it.
1. Periodically, a client should query it's ring1 peers. A configuration file,whose implementation is out of the scope of this document, lists the URLs of the friends websites. For each URL, the client will download its `$DOCROOT/self.txt` file. This information is used to generate the client's `hop1.txt`. This list is trustworthy for other peers to download, because the client directly fetched each details file from its respective owner.
2. Then, the client tries to generate the `hop2.txt` file. The client fetches one `hop1.txt` file for each other peers in the client's hop1 list. Data is concatenated using the same format used to generate `1hop.txt` previously
  Because our ring1 peers are verifying the authenticity of their own ring1 peers, we can trust the ring2 records.
  - Some peers may list some common websites in their ring1 peers. In this case, the generated ring2 list has to be deduplicated.
  - If the client's ring1 contains a website which is also present in a `hop1.txt` file of another server, in case of a details mismatch, the information the client fetched directly in the previous step takes precedence as it's more recently updated and more trustworthy, as it has been fetched directly.
4. (optional, slow and discouraged) our client can query the ring2+ peers to verify that our ring1 peers are not lying. This should be done sparingly, as each time the hop level is incresed, the number of nodes to fetch in the worst case scenario increases exponentially.

## Limits

It's HIGHLY suggested to limit the number of entries in each peer's ring1 list. The number of peers in the hop_n ring, where _n_ is a natural number and indicates the hop level, and K indicates the number of entries of each ring1 peer, in case of zero duplicates, grows exponentially as:

> N = K^n

This means that, even if the ring1 entries list of each peer is truncated to 10 urls, the worst case scenario for the ring2 list amounts to 100 nodes, and the ring3 worst case scenario will amount at 1000 nodes.

Obviously, this number is limited by the total number of nodes, and the ring_n list length approaches the total of the whole network.
This is a contradiction to the goal of a webring. A webring makes sense when a small valuable subset of the network is included, but the overall network can still be traversed if referenced by some of the nodes.
Instead, if the friends nodes list becomes too big, some problems arise:

- more network traffic is needed to update the hop_n files
- limiting the ring1 size means the website owner has to keep the quality bar high, and favouring websites which are actually valuable and trustworthy
- if each node references too many nodes, the network can not be trusted anymore. In this situtation, if one peer adds a malicious website to its ring, that website would be spread into the whole network

It's suggested to limit the ring size. Software implementations are authorized to truncate the last elements of a fetched list, if they're too many. A value from 8 to 10 entries for ring1 is suggested.


## Displaying as html

A poor example: 

```
**hop1 peers**

- me
- my friend Bob
- my friend Sally

**hop2 peers**

- my friend Bob's friend Thomas
- my friend Bob's friend Emily
- my friend Sally's friend Chloe
- my friend Sally's friend Cole
```

# secondary features

Not part of the standard. Cool things that can be built on top of the network. 

- uptime checker for our friends 

